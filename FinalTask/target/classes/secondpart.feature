Feature: Smoke
  As a user
  I want to test site main functionality
  So that I can be sure that I can search and by products


  Scenario Outline: Check word presence
    Given User opens '<lorem>' loremPage
    And User Selects '<option>' in selector
    And User enters numbers from '<ListOfWordsNumber>' into the number field and Verifyies the result has entered number of selected item for each value from the list
    And User Selects different '<secondOption>' in selector
    And User enters numbers from '<ListOfWordsNumberSecondTest>' into the number field again and Verifyies the result has entered number of selected item for each value from the list


    Examples:
      | lorem               | option | ListOfWordsNumber | secondOption | ListOfWordsNumberSecondTest |
      | https://lipsum.com/ | word   | 11                | bytes        | 3                           |
      | https://lipsum.com/ | word   | -1                | bytes        | 0                           |
      | https://lipsum.com/ | word   | 0                 | bytes        | -5                          |
      | https://lipsum.com/ | word   | 5                 | bytes        | 11                          |
      | https://lipsum.com/ | word   | 20                | bytes        | 22                          |

  Scenario Outline: verify the checkbox
    Given User opens '<lorem>' loremPage
    And User unchecks start with Lorem Ipsum checkbox
    And User presses Generate Lorem Ipsum
    And User Verify that result no longer starts with Lorem ipsum

    Examples:
      | lorem               |
      | https://lipsum.com/ |


  Scenario Outline: check that randomly generated text paragraphs contain the word "lorem" with probability of more than 40%:
    Given User opens '<lorem>' loremPage
    And User Press Generate Lorem Ipsum <number> times and check the average number of paragraphs containing the word lorem
    Examples:
      | lorem               | number |
      | https://lipsum.com/ | 10     |
