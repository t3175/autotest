package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoremFeedPage extends BasePage {

    private final String paragraphTemplate = "//div[@id = 'Content']//p[";
    private double totalNumberOfLoremWords;
    private double totalNumberOfParagraphs;
    @FindBy(xpath = "//div[@id='generated']")
    private WebElement generatedSummary;

    public LoremFeedPage(WebDriver driver) {
        super(driver);
    }

    public void getNumberOfLoremWordsInGeneratedParagraphs (int numberOfParagraphs) {
        for (int i = 1; i <= numberOfParagraphs; i++) {
            String elementXpath = paragraphTemplate + i + "]";
            if (driver.findElement(By.xpath(elementXpath)).getText().
                    toLowerCase().contains("lorem")) totalNumberOfLoremWords ++;
        }
    }

    public void verifyThatParagraphContainsText(int paragraphNumber, String expectedText) {
        String elementXpath = paragraphTemplate + paragraphNumber + "]";
        Assert.assertTrue(driver.findElement(By.xpath(elementXpath)).getText().contains(expectedText));
    }

    public void verifyThatParagraphDoesNotContainText(int paragraphNumber, String expectedText) {
        String elementXpath = paragraphTemplate + paragraphNumber + "]";
        Assert.assertFalse(driver.findElement(By.xpath(elementXpath)).getText().
                toLowerCase().contains(expectedText));
    }


    public void verifyTotalNumberOfLoremWordsAfterSeveralTests () {
        System.out.println(totalNumberOfParagraphs/totalNumberOfLoremWords);
        Assert.assertTrue(totalNumberOfParagraphs/totalNumberOfLoremWords <= 3.0);
    }

    public void setTotalNumberOfParagraphs(int totalNumberOfParagraphs) {
        this.totalNumberOfParagraphs = totalNumberOfParagraphs;
    }

    public void verifyThatResultContainsSelectedNumberAndCorrectOption (String number, String option) {
        if (Integer.valueOf(number) <=0) number = "1";
        Assert.assertTrue(generatedSummary.getText().toLowerCase().contains(number));
        Assert.assertTrue(generatedSummary.getText().toLowerCase().contains(option));
    }
}
