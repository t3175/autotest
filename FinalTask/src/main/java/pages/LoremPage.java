package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class LoremPage extends BasePage {

    private List<Integer> numberOfWordsList;
    @FindBy(xpath = "//p[contains(.,'Lorem Ipsum - это')]")
    private WebElement firstParagraph;
    @FindBy(xpath = "//input[contains(@name,'generate')]")
    private WebElement generateLoremIpsumButton;
    @FindBy(xpath = "//input[contains(@name,'start')]")
    private WebElement startWithLoremIpsumCheckBox;
    private String checkBoxTemplate = "//label[contains(.,'";
    @FindBy(xpath = "//input[contains(@name,'amount')]")
    private WebElement numberOfElementsToGenerateField;
    private String currentOption;


    public void openURL(String URL) {
        driver.get(URL);
    }


    public LoremPage(WebDriver driver) {
        super(driver);
    }

    public void checkThatFirstParagraphContainsText(String text) {
       Assert.assertTrue(firstParagraph.getText().toLowerCase().contains(text));
    }

    public void clickGenerateLoremIpsumButton() {
        generateLoremIpsumButton.click();
    }

    public void clickOnStartWithLoremIpsumCheckbox() {
        startWithLoremIpsumCheckBox.click();
    }

    public void selectWhatToGenerate(String option) {
        driver.findElement(By.xpath(checkBoxTemplate + option + "')]")).click();
        currentOption = option;
    }

    public void setNumberOfWordsList(List<Integer> numberOfWordsList) {
        this.numberOfWordsList = numberOfWordsList;
    }

    public void setNumberOfElementsToGenerateAndGenerateLorem (String numberOfElements) {
        numberOfElementsToGenerateField.clear();
        numberOfElementsToGenerateField.sendKeys(numberOfElements);
        clickGenerateLoremIpsumButton();
    }

    public String getCurrentOption() {
        return currentOption;
    }
}
