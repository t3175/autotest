package stepdefinitions;


import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.bs.A;
import io.cucumber.java.en.And;
import manager.PageFactoryManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.*;

import java.util.List;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;
import static java.lang.Thread.sleep;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static sun.plugin.javascript.navig.JSType.URL;

public class DefinitionSteps {

    private static final long DEFAULT_TIMEOUT = 60;
    WebDriver driver;
    PageFactoryManager pageFactoryManager;
    LoremPage loremPage;
    LoremFeedPage loremFeedPage;
    private static final int DEFAULT_PARAGRAPHS_NUMBER = 5;

    @Before
    public void testsSetUp() {
        chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pageFactoryManager = new PageFactoryManager(driver);
    }


    @And("User opens {string} loremPage")
    public void openLoremPage(String URL) {
        loremPage = pageFactoryManager.getLoremPage();
        loremPage.openURL(URL);

    }

    @And("User switches to {string} language using one of the links")
    public void switchLanguage(String language) {
        loremPage.switchLanguage(language);
    }

    @And("the text of the first  element, which is the first paragraph, contains the required {string}")
    public void verifyWordPresence(String word) {
        loremPage.checkThatFirstParagraphContainsText(word);
    }

    @And("User presses Generate Lorem Ipsum")
    public void clickGenerateLoremIpsum() {
        loremPage.clickGenerateLoremIpsumButton();
        loremPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
    }

    @And("the {int} paragraph starts with {string}")
    public void verifyGenerationResults(int paragraphNumber, String expectedText) {
        loremFeedPage = pageFactoryManager.getLoremFeedPage();
        loremFeedPage.verifyThatParagraphContainsText(paragraphNumber, expectedText);

    }

    @And("User deactivates start with Lorem Ipsum checkbox")
    public void clickStartWithLoremIpsumCheckbox() {
        loremPage.clickOnStartWithLoremIpsumCheckbox();
    }

    @And("User Verify that result no longer starts with Lorem ipsum")
    public void checkThatResultDoesNotContainLoremWord() {
        loremFeedPage = pageFactoryManager.getLoremFeedPage();
        loremFeedPage.verifyThatParagraphDoesNotContainText(1, "Lorem ipsum");
    }


    @And("User Press Generate Lorem Ipsum {int} times and check the average number of paragraphs containing the word lorem")
    public void checkAverageNumberOfLoremWord(int requiredGenerationNumber) {
        loremFeedPage = pageFactoryManager.getLoremFeedPage();
        loremFeedPage.setTotalNumberOfParagraphs(DEFAULT_PARAGRAPHS_NUMBER * requiredGenerationNumber);
        for (int i = 0; i < requiredGenerationNumber; i++) {
            loremPage.clickGenerateLoremIpsumButton();
            loremFeedPage.getNumberOfLoremWordsInGeneratedParagraphs(DEFAULT_PARAGRAPHS_NUMBER);
            loremFeedPage.navigateBack();
        }

        loremFeedPage.verifyTotalNumberOfLoremWordsAfterSeveralTests();
    }


    @And("User Selects {string} in selector")
    public void selectOptionToGenerate(String option) {
        loremPage.selectWhatToGenerate(option);
        loremFeedPage = pageFactoryManager.getLoremFeedPage();
    }

    @And("User enters numbers from {string} into the number field and checks the result has entered number of selected item for each value from the list")
    public void enterNumbersAndVerifyTheResults(String currentNumber) {
        loremPage.setNumberOfElementsToGenerateAndGenerateLorem(currentNumber);
        loremFeedPage.verifyThatResultContainsSelectedNumberAndCorrectOption(currentNumber, loremPage.getCurrentOption());
        loremFeedPage.navigateBack();

    }

    @And("User Selects different {string} in selector")
    public void selectDifferentOption(String secondOption) {
        loremPage.selectWhatToGenerate(secondOption);
    }

    @And("User enters numbers from {string} into the number field again and checks the result has entered number of selected item for each value from the list")
    public void enterNumbersAndVerifyTheResultSecondTest(String wordsNumber) {

            loremPage.setNumberOfElementsToGenerateAndGenerateLorem(wordsNumber);
            loremFeedPage.verifyThatResultContainsSelectedNumberAndCorrectOption(wordsNumber, loremPage.getCurrentOption());

    }


    @After
    public void tearDown() {
        driver.close();
    }

}
