package manager;

import org.openqa.selenium.WebDriver;
import pages.*;

public class PageFactoryManager {

    WebDriver driver;

    public PageFactoryManager(WebDriver driver) {
        this.driver = driver;
    }

    public LoremPage getLoremPage() {
        return new LoremPage(driver);
    }

    public LoremFeedPage getLoremFeedPage () {
        return new LoremFeedPage(driver);
    }

}
