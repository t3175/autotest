Feature: Language and default setting features

  Scenario Outline: Check word presence
    Given User opens '<lorem>' loremPage
    And User switches to '<language>' language using one of the links
    And the text of the first  element, which is the first paragraph, contains the required '<word>'

    Examples:
      | lorem               | language | word |
      | https://lipsum.com/ | Pyccкий  | рыба |


  Scenario Outline: verify that default setting result in text starting with Lorem ipsum:

    Given User opens '<lorem>' loremPage
    And User presses Generate Lorem Ipsum
    And the <number> paragraph starts with '<Text>'

    Examples:
      | lorem               | number | Text                                                    |
      | https://lipsum.com/ | 1      | Lorem ipsum dolor sit amet, consectetur adipiscing elit |